package playing.with.lippia.reportserver.handler.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}